# SpaceVim Installer - Rocky Linux

This repository contains a collection of scripts that allow you to install the SpaceVim editor for a Rocky Linux system.
In addition, there are scripts to enable support for multiple languages by installing all required packages.

## How to Use

### Pre-Installation

In order to run the installer flawlessly you need to enable `snap-store` and `node` (root-less global install) before:

```sh
# snap-store
sudo yum install -y epel-release
sudo yum install -y snapd
sudo systemctl enable --now snapd.socket
sudo ln -s /var/lib/snapd/snap /snapd

# node-version-manager
curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.39.0/install.sh | bash
```

Once the upon commands ran, you need to reboot.

Then you need to install the latest lts version of node and npm:

```sh
nvm install --lts
```

### Installation 

To run the installer simply execute the following command:

```sh
./install.sh
```

### Run the editor

SpaceVim can be run as usual by executing either `vim` or `nvim`.

> Neovim will be installed automatically, but isn't the recommended base editor.

### Uninstall

If you want to uninstall SpaceVim entierly, you can run the following command:

```sh
./uninstall.sh
```

> Note: This script uninstall SpaceVim only, not all the packages installed during the installation.

## Configured Languages

- [x] C / C++
- [x] Dockerfile
- [x] JavaScript
- [x] Python
- [x] sh
- [x] toml
- [x] TypeScript
- [x] Vim

## Configured Layers

The following layers are also enabled and configured to provide maximum features.

- [x] autocomplete
- [x] checkers
- [x] colorscheme
- [x] core#statusline
- [x] core#tabline
- [x] core
- [x] cscope
- [x] debug
- [x] default
- [x] edit
- [x] format
- [x] git
- [x] gtags
- [x] ui
- [x] VersionControl
- [x] lsp
