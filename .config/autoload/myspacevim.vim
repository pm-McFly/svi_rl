function! myspacevim#before() abort

"====[ Toggle visibility of naughty characters ]============

" Make naughty characters visible...
" (uBB is right double angle, uB7 is middle dot)
  set lcs=tab:»·,trail:␣,nbsp:˷
  highlight InvisibleSpaces ctermfg=Black ctermbg=Black
  call matchadd('InvisibleSpaces', '\S\@<=\s\+\%#\ze\s*$')

  augroup VisibleNaughtiness
    autocmd!
    autocmd BufEnter  *       set list
    autocmd BufEnter  *       set list
    autocmd BufEnter  *.txt   set nolist
    autocmd BufEnter  *.vp*   set nolist
    autocmd BufEnter  *       if !&modifiable
    autocmd BufEnter  *           set nolist
    autocmd BufEnter  *       endif
  augroup END

"====[ Highlight more characters in JSX files ]============
  let g:vim_jsx_pretty_colorful_config = 1 " default 0

endfunction

function! myspacevim#after() abort
endfunction
