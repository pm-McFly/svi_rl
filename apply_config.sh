#!/usr/bin/env bash

# Copy configuration file
cp ./.config/init.toml ~/.SpaceVim.d/init.toml

# Copy 'autoload' functions
cp ./.config/autoload/ ~/.SpaceVim.d/ -r
