#!/usr/bin/env bash

# Copy configuration file
cp ./.config/init.toml ~/.SpaceVim.d/init.toml

# Copy 'autoload' functions
cp ./.config/autoload/ ~/.SpaceVim.d/ -r

for f in ./config/*.sh; do
  bash "$f" || break  # execute successfully or break
  # Or more explicitly: if this execution fails, then stop the `for`:
  # if ! bash "$f"; then break; fi
done
