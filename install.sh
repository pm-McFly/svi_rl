#!/usr/bin/env bash

# Prepare system for installation
sudo ./prepare.sh

# Install SpaceVim
curl -sLf https://spacevim.org/install.sh | bash

vim

# Post install script
./post-install.sh
