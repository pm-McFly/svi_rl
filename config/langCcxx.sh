#!/usr/bin/env bash

# Install libclang and other clang tools
sudo yum install -y		\
	clang			\
	clang-devel		\
	clang-tools-extra	\
	clang-analyzer		\
	clang-libs
