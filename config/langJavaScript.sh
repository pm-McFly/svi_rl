#!/usr/bin/env bash

# Install JS / TS language server
npm install -g javascript-typescript-langserver

# Install linter
npm install -g eslint-cli

# Install formatter
npm install -g js-beautify
