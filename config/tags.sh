#!/usr/bin/env bash

# Install tags generation tools

sudo yum install -y	\
	ctags

sudo yum install -y --enablerepo=powertools	\
	ctags-etags

