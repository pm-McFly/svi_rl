#!/usr/bin/env bash

# Install python
sudo yum install -y	\
	python3		\
	python3-devel

# Install Python language server
pip3 install --user python-language-server
npm install -g pyright

# Install checker
pip3 install --user pylint

# Install formatters
pip3 install --user yapf
pip3 install --user autoflake
pip3 install --user isort
pip3 install --user coverage

