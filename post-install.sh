#!/usr/bin/env bash

# Install python3
sudo yum install -y	\
	python3

# Install python support for NeoVim
pip3 install --user pynvim

# Configure SpaceVim
./configure.sh
