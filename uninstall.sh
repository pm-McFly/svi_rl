curl -sLf https://spacevim.org/install.sh | bash -s -- --uninstall
rm -rf ~/.SpaceVim*
rm -rf ~/.cache/neosnippet/
rm -rf ~/.cache/SpaceVim/
rm -rf ~/.cache/vimfiles/
