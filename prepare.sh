#!/usr/bin/env bash

if [ "$EUID" -ne 0 ]
  then echo "Please run this script with sudo"
  exit
fi

# Base system requirements
yum install -y	\
	epel-release

# Base tools requirements
yum install -y	\
	vim	\
	neovim	\
	python3-neovim

# Installation dependencies
yum install -y	\
	git	\
	curl	\
	make	\
	python3

sudo ln -s /usr/bin/python3 /usr/bin/python
